package node

import (
	"fmt"
	"time"
)

type Node interface {
	Depth() int
	setDepth(int)
	Parent() Node
	setParent(Node)
	Value() interface{}
	String() string
	//AddNeighbour(Node)
	//Neighbours() []Node
	//WriteNeighbours(chan Node)
	Neighbours() []Node
	Final() bool
}

type node struct {
	depth  int
	parent Node
	//neighbours []Node
}

func (n *node) Depth() int {
	return n.depth
}

func (n *node) setDepth(d int) {
	n.depth = d
}

func (n *node) Parent() Node {
	return n.parent
}

func (n *node) setParent(nn Node) {
	n.parent = nn
}

// func (n *node) AddNeighbour(nn Node) {
// 	n.neighbours = append(n.neighbours, nn)
// }

// func (n *node) Neighbours() []Node {
// 	return n.neighbours
// }

func printShortestPath(n Node) {
	path := make([]string, n.Depth())
	current := n
	for i := 1; i <= n.Depth(); i++ {
		path[n.Depth()-i] = current.String()
		current = current.Parent()
	}
	for _, s := range path {
		fmt.Println(s)
	}
}

// IndexedGraph holds a map of the Nodes
type IndexedGraph struct {
	index                   map[interface{}]Node
	maxDepth                int
	candidates              chan Node
	probablyValidCandidates chan Node
	unexpandedNodes         chan Node
	expandableNodes         chan Node
	sorterStarving          chan bool
	result                  chan Node
	shortest                Node
	indices                 [][]Node
	smallestNonEmptyIndex   int
	sneiCtl                 chan int
}

func NewIndexedGraph(max int) *IndexedGraph {
	g := new(IndexedGraph)
	g.index = make(map[interface{}]Node)
	g.candidates = make(chan Node)
	g.probablyValidCandidates = make(chan Node)
	g.unexpandedNodes = make(chan Node)
	g.expandableNodes = make(chan Node)
	g.sorterStarving = make(chan bool)
	g.result = make(chan Node)
	g.indices = make([][]Node, max+1) // The slice of slice of nodes
	go func() {
		// To edit this field in a thread safe way, use g.SetSNEI()
		for true {
			g.smallestNonEmptyIndex = <-g.sneiCtl
		}
	}()
	return g
}

// SetSNEI esits the Smallest Non Empty Index in a thread safe way
func (g *IndexedGraph) SetSNEI(snei int) {
	g.sneiCtl <- snei
}

// CircularWorker is a three state state machine.
// It can either be:
// - Waiting for input
// - Working on the input it just got
// - Starved and ready to quit
func CircularWorker(input chan Node, doSomething func(Node)) chan bool {
	ctl := make(chan bool)
	go func() {
		for true {
			select {
			// Waiting
			case n := <-input:
				// Working
				doSomething(n)
			case ctl <- true:
				select {
				// Starved
				case <-ctl:
					return
				case n := <-input:
					//Working
					doSomething(n)
				}
			}
		}
	}()
	return ctl
}

// DepthChecker just makes sure the given Node's depth is not higher
// than the maximum allowed depth.
// Valid nodes are sent to the output channel.
func (g *IndexedGraph) DepthChecker(candidate Node, output chan Node) {
	if candidate.Depth() <= g.maxDepth {
		output <- candidate
	}
}

// TreeBuilder will check the given Node and add it
// to the IndexedGraph if it is valid.
// Added or modified Nodes are sent to the output channel.
func (g *IndexedGraph) TreeBuilder(candidate Node, output chan Node) {
	n, alreadyThere := g.index[candidate.Value()]
	if !alreadyThere {
		g.index[candidate.Value()] = candidate
		if candidate.Final() {
			//We found our answer
			g.shortest = candidate
			printShortestPath(candidate)
			g.maxDepth = candidate.Depth()
		}
		output <- candidate
	} else if n.Depth() > candidate.Depth() {
		n.setDepth(candidate.Depth())
		n.setParent(candidate.Parent())
		output <- n
	}
}

// findNext returns the next Node one ought to send
// to be expanded. Also return the smallest index of a non empty slice
func findNext(indices *[][]Node, max int, first int) (Node, int) {
	if first < 0 {
		for i := 0; i <= max; i++ {
			if len((*indices)[i]) > 0 {
				first = i
				break
			}
		}
		if first < 0 {
			return nil, -1 //No node to expand for now
		}
	}
	//We first try to pop a value out of the smallest depth slice that has one
	current := (*indices)[first]
	l := len(current)
	answer := current[l-1]
	(*indices)[first] = (*indices)[first][:l-1]
	if l == 1 {
		// We emptied this slice, we should scan, next time
		return answer, -1
	}
	return answer, first
}

// KillerBuffer checks if the given node need to be expanded
// and if so stores it immediately, storing by depth.
func (g *IndexedGraph) KillerBuffer(candidate Node) {
	if candidate.Depth() >= g.maxDepth {
		return // We don't need to expand nodes,
		// the neighours will be too deep.
	}
	if candidate.Depth() < g.smallestNonEmptyIndex {
		g.SetSNEI(candidate.Depth())
	}
	g.indices[candidate.Depth()] =
		append(g.indices[candidate.Depth()], candidate)
}

func (g *IndexedGraph) FeedExpander() bool {
	// Try to find something to expand
	next, smallestNonEmptyIndex := findNext(&g.indices, g.maxDepth,
		g.smallestNonEmptyIndex)
	if next == nil {
		return false
	}
	if smallestNonEmptyIndex != g.smallestNonEmptyIndex {
		g.SetSNEI(smallestNonEmptyIndex)
	}
	// Expand it
	g.expandableNodes <- next
	return true
}

func (g *IndexedGraph) CircularController() {
	for true {
		// If there is nothing to expand
		if g.FeedExpander() {
			continue
		}
		// Make sure the Expander is starved
		<-g.ExpanderCtl
		// Try again
		if g.FeedExpander() {
			continue
		}
		// Make sure the DepthChecker is starved too
		<-g.DepthCheckerCtl
		// Try again
		if g.FeedExpander() {
			continue
		}
		// Make sure the TreeBuilder is starved too
		<-g.TreeBuilderCtl
		// Try again
		if g.FeedExpander() {
			continue
		}
		// Make sure the KillerBuffer is starved too
		<-g.KillerBufferCtl
		// Everybody is starved, we should quit.
		g.ExpanderCtl <- true
		g.DepthCheckerCtl <- true
		g.TreeBuilderCtl <- true
		g.KillerBufferCtl <- true

	}
}

// SorterKiller orders nodes from the unexpandedNodes channel
// by their depth and kills those that should not be expanded
func (g *IndexedGraph) SorterKiller() {
	// to expand, stored by depth
	smallestNonEmptyIndex := -1 // index of the first non empty slice
	var next Node               // next Node to send
	for true {                  // As long as FIXME
		//
		var unexpanded Node
		// Find something to send
		if next == nil {
			next, smallestNonEmptyIndex = findNext(&indices, g.maxDepth,
				smallestNonEmptyIndex)
		}
		if next == nil || next.Depth() >= g.maxDepth {
			// If there is nothing to send to the Expander,
			//fmt.Printf("DBG: Sorter has nothing to send\n")
			unexpanded = <-g.unexpandedNodes
			// just read from the Builder
		} else {
			// Either send to the Expander or read from the Builder
			//fmt.Printf("DBG: Sorter can send %v\n", next)
			select {
			case unexpanded = <-g.unexpandedNodes:
			case g.expandableNodes <- next:
				//fmt.Printf("DBG: Sorter sent %v\n", next)
				next = nil
			}
		}
		if unexpanded != nil {
			// If we received
			//fmt.Printf("DBG: Sorter received %v\n", unexpanded)
			if unexpanded.Depth() >= g.maxDepth {
				continue
			}
			if unexpanded.Depth() < smallestNonEmptyIndex {
				smallestNonEmptyIndex = unexpanded.Depth()
				next = nil
			}
			indices[unexpanded.Depth()] =
				append(indices[unexpanded.Depth()], unexpanded)
		}
	}
}

// Expander writes to the output channel
// the neighbours of the given Node
func (g *IndexedGraph) Expander(expandable Node, output chan Node) {
	for _, neighbour := range expandable.Neighbours() {
		output <- neighbour
	}
}

// Scaffolder creates all the channels and launch the goroutines
// that write and reads to and from them.
func (g *IndexedGraph) Scaffolder(root Node) chan Node {
	// Launch the Builder, which will block waiting for a node or
	// for a signal of the the SorterKiller that the latter is starving
	go g.TreeBuilder()
	// Send the root until the builder gets it
	//fmt.Printf("DBG Scaffolder sending\n")
	g.candidates <- root
	//fmt.Printf("DBG Scaffolder has sent\n")
	// Now the Builder is waiting to send to the SorterKiller but
	// can't because we haven't launched it yet.
	// Let's correct that
	go g.SorterKiller()
	// We can now launch the rest
	//go g.TreeAssesser()
	go g.Expander()
	go func() {
		for true {
			fmt.Printf("%v elements in the graph\n", len(g.index))
			time.Sleep(time.Second)
		}
	}()
	return g.result
}

func (g *IndexedGraph) AsDot() string {
	answer := ""
	for _, n := range g.index {
		for _, nn := range n.Neighbours() {
			answer += n.String() + "->" + nn.String() + ";\n"
		}
	}
	return answer
}
