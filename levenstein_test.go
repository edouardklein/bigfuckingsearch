package node

import (
	"fmt"
	"testing"
)

type nodeString struct {
	node
	value string
}

func (n *nodeString) Value() interface{} {
	return n.value
}

func (n *nodeString) String() string {
	return n.value
}

func (n *nodeString) Final() bool {
	return n.value == "PUTE"
}

func (n *nodeString) Neighbours() []Node {
	ndepth := n.Depth() + 1
	var answer []Node
	for _, char := range "ABCDEFGHIJKLMNOPQRSTUVWXYZ" {
		for pos, _ := range n.value {
			nvalue := n.value[:pos] + string(char) + n.value[pos+1:]
			nnode := node{depth: ndepth, parent: n} //, neighbours: nil}
			neighbour := nodeString{
				nnode,
				nvalue}
			answer = append(answer, &neighbour)
		}
	}
	return answer
}

func TestDoraToPute(t *testing.T) {
	dora := nodeString{node{}, "DORA"}
	for _, a := range dora.Neighbours() {
		fmt.Println(a.String())
	}
	g := indexedGraph{}
	g.maxDepth = 5
	r := g.Scaffolder(&dora)
	fmt.Println("Main waiting on result")
	fmt.Println(<-r)
}
